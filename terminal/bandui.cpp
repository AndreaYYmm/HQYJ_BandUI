#include "bandui.h"


#include <QDebug>
#include <QDateTime>
#include <QColor>
#include <QString>
BandMainUI::BandMainUI()
{
    InitMainPage();
}


void BandMainUI::InitMainPage()
{
    leTime = new QLineEdit(this);
    leData = new QLineEdit(this);
    leWeek = new QLineEdit(this);
    leTemp = new QLineEdit(this);
    leElec = new QLineEdit(this);
    leStep = new QLineEdit(this);
    //bt = new QPushButton(this);

    QDateTime time = QDateTime::currentDateTime();
    qDebug() << "week : " << time.toString("ddd") << endl;

    this->setWindowFlags(Qt::Dialog);
    this->setWindowTitle("表盘主页");
    this->setFixedSize(311, 486);
    this->setAutoFillBackground(true);
    QPalette palette;
    QPixmap pixmap("://images/taikongren.png");
    palette.setBrush(QPalette::Window, QBrush(pixmap));
    this->setPalette(palette);

    leTemp->move(0,0);
    leTemp->resize(30,32);
    leTemp->setFont(QFont("YouYuan", 13, QFont::Bold));
    leTemp->setAutoFillBackground(true);
    leTemp->setStyleSheet("border :1px ;background-color: rgba(0,0,0,0);color: rgb(31,32,52)");
    leTemp->setText("28");
    leTemp->setReadOnly(true);

    leElec->move(250,0);
    leElec->resize(50,30);
    leElec->setFont(QFont("YouYuan", 12, QFont::Bold));
    leElec->setAutoFillBackground(true);
    leElec->setStyleSheet("border :1px ;background-color: rgba(0,0,0,0);color: rgb(31,32,52)");
    leElec->setText("100");
    leElec->setReadOnly(true);

    leTime->move(0,40);
    leTime->resize(150,150);
    leTime->setFont(QFont("YouYuan", 42, QFont::Bold));
    leTime->setAutoFillBackground(true);
    leTime->setStyleSheet("border :1px ;background-color: rgba(0,0,0,0);color: rgb(31,32,52)");
    leTime->setReadOnly(true);
    leTime->setText(time.toString("hh:mm"));

    leData->move(188, 75);
    leData->resize(130,60);
    leData->setFont(QFont("YouYuan", 23, QFont::Bold));
    leData->setAlignment(Qt::AlignRight);
    leData->setAutoFillBackground(true);
    leData->setStyleSheet("border :1px ;background-color: rgba(0,0,0,0);color: rgb(31,32,52)");
    leData->setText(time.toString("MM-dd "));
    leData->setReadOnly(true);

    leWeek->move(188, 126);
    leWeek->resize(130,50);
    leWeek->setFont(QFont("YouYuan", 16, QFont::Bold));
    leWeek->setAutoFillBackground(true);
    leWeek->setAlignment(Qt::AlignRight);
    leWeek->setStyleSheet("border :1px ;background-color: rgba(0,0,0,0);color: rgb(31,32,52)");
    leWeek->setText(time.toString("ddd "));
    leWeek->setReadOnly(true);


    leStep->move(55, 160);
    leStep->resize(130,50);
    leStep->setFont(QFont("YouYuan", 16, QFont::Bold));
    leStep->setAutoFillBackground(true);
    leStep->setAlignment(Qt::AlignLeft);
    leStep->setStyleSheet("border :1px ;background-color: rgba(0,0,0,0);color: rgb(31,32,52)");
    leStep->setText(time.toString("00000"));
    leStep->setReadOnly(true);



    QString str = time.toString("yyyy-MM-dd hh:mm:ss");

    qDebug() << str << endl;

    leTime->move(0,86);
    leTime->resize(312,50);


    str = time.toString("hh:mm");
    qDebug() << str << endl;

    str = time.toString("MM-dd");
    qDebug() << str << endl;


}



void BandMainUI::mouseReleaseEvent(QMouseEvent *event)
{
   // emit BandMainUI::mainClicked();
    Q_UNUSED(event);
    emit  ClickedSignalFirst();
    qDebug() << "BBandMainUI::mouseReleaseEvent(QMouseEvent *event)" << endl;

}



BandStepUI::BandStepUI()
{
    InitStepPage();
}


void BandStepUI::InitStepPage()
{
    lb = new QLabel(this);
    gif = new QMovie("://images/run.gif");
    le = new QLineEdit(this);
    //bt = new QPushButton(this);


    QColor color(0,0,0,255);
    this->setFixedSize(311, 486);
    this->setStyleSheet("border :1px ;background-color: rgba(31,32,52,1)");
    //this->setWindowOpacity(0.8);

   // this->setWindowFlags(Qt::Dialog | Qt::FramelessWindowHint);

    lb->setScaledContents(true);
    lb->setMovie(gif);
    gif->setBackgroundColor(color);
    gif->start();

    le->move(100,380);
    le->resize(111,50);
    le->setFont(QFont("YouYuan", 24, QFont::Bold));
    le->setAutoFillBackground(true);
    le->setStyleSheet("border :1px ;background-color: rgba(0,0,0,0);color: rgb(31,32,52)");
    le->setAlignment(Qt::AlignCenter);

    le->setText("00000");
    le->setReadOnly(true);


    //lb->show();
}


void BandStepUI::mouseReleaseEvent(QMouseEvent *event)
{
   // emit BandStepUI::stepClicked();
    Q_UNUSED(event);
    emit ClickedSignal();
    qDebug() << "BandStepUI::mouseReleaseEvent(QMouseEvent *event)" << endl;

}

