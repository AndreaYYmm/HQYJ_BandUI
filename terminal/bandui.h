#ifndef BANDUI_H
#define BANDUI_H
#include <QWidget>
#include <QDialog>
#include <QLineEdit>
#include <QDateTime>
#include <QLabel>
#include <QMovie>
#include <QPushButton>
class BandMainUI : public QDialog
{
    Q_OBJECT
public:

    QLineEdit* leTime ;
    QLineEdit* leData ;
    QLineEdit* leWeek ;
    QLineEdit* leTemp ;
    QLineEdit* leElec ;
    QLineEdit* leStep ;
    QPushButton* bt;
    //QDateTime time ;

    void InitMainPage();
    void mouseReleaseEvent(QMouseEvent *event);

public:
    explicit BandMainUI();

signals:
    void ClickedSignalFirst(void);

};

class BandStepUI:public QDialog
{
    Q_OBJECT
public:
    QLabel *lb;
    QMovie *gif;
    QLineEdit *le;
    QPushButton* bt;

    void InitStepPage();
    void mouseReleaseEvent(QMouseEvent *event);

public:
    explicit BandStepUI();

signals:
    void ClickedSignal(void);

};


#endif // BANDUI_H
