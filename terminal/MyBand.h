#ifndef MYBAND_H
#define MYBAND_H

#include "bandui.h"
#include <QStackedLayout>

class MyBand : public QWidget
{
    Q_OBJECT
public:
    BandMainUI *first;
    BandStepUI *step;

public:
    MyBand();
private slots:
    void HandOver();  //切换页面
};

#endif // MYBAND_H
