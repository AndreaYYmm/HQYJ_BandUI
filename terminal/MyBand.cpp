#include "MyBand.h"
#include "bandui.h"

#include <QStackedLayout>
#include <QStackedLayout>
#include <QTimer>


MyBand::MyBand()
{
    first = new BandMainUI;
    step = new BandStepUI;

    QStackedLayout * sLayout = new QStackedLayout();
    sLayout->addWidget(first);
    sLayout->addWidget(step);

    setLayout(sLayout);

    first->show();
    step->show();
#if 0
    QTimer * timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(HandOver()));
    timer->start(2000);
#else
    connect(this->first, &BandMainUI::ClickedSignalFirst, this, &MyBand::HandOver);
    connect(this->step, &BandStepUI::ClickedSignal, this, &MyBand::HandOver);
#endif

}


void MyBand::HandOver()
{
    QStackedLayout * sLayout = dynamic_cast<QStackedLayout*>(layout());

    if(sLayout != nullptr) {
        int index = (sLayout->currentIndex() + 1) % sLayout->count();
        sLayout->setCurrentIndex(index);
    }
}
