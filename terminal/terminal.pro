QT += widgets serialport widgets
requires(qtConfig(combobox))

TARGET = terminal
TEMPLATE = app

SOURCES += \
    MyBand.cpp \
    bandui.cpp \
    main.cpp \
    mainwindow.cpp \
    settingsdialog.cpp \
    console.cpp

HEADERS += \
    MyBand.h \
    bandui.h \
    mainwindow.h \
    settingsdialog.h \
    console.h

FORMS += \
    mainwindow.ui \
    settingsdialog.ui

RESOURCES += \
    terminal.qrc

target.path = $$[QT_INSTALL_EXAMPLES]/serialport/terminal
INSTALLS += target

RC_ICONS = terminal.ico
